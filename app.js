const express = require("express");
var fs = require('fs');

const app = express();

app.listen(process.env.PORT || 3000, function(){
    console.log("App is running on port 3000");
});

app.use(express.static("html"));

app.get("/", function(req, res){
    fs.readFile('html/sample.html', function(error, data){
        if(error){
            console.log(error);
        }else{
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.end(data);
        }
    })
});